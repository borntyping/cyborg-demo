<!--

## Background

- Systems administration
- Alerting
- Service monitoring

## Reactive Monitoring system

- More generic approach
- Rules
    - Conditions
    - Actions
- Extras
    - Resources
    - Loggers

## Demo

Uses some components that are not part of the project.

Riemann dashboard is used to display information from Riemann.
Supermann is used to collect more information than project does.

-->

## Example 1: Memory limit

A set of processes (mutiny_X) that consume memory.

**When** they use more than 500Mb of memory
**Then** kill the process

## Example 2: HTTP service

A rule randomly kills a HTTP service with a 1 in 20 chance each tick.

**When** `[[ $((RANDOM % 20)) != 0 ]]`
**Then** stop the service

Another rule starts the service if it fails a HTTP check.

**When** `http GET http://localhost:5000`
**Then** start the service

Both of these use existing command line tools.
