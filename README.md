cyborg-demo
===========

A self contained demo for [Cyborg](https://github.com/borntyping/cyborg) using [Mutiny](https://github.com/borntyping/mutiny), [Docker](https://www.docker.com/) and [Supervisord](http://supervisord.org/).

Usage
-----

Start the demonstration with `./demo.sh`, then `q` to exit `multitail` and kill the docker container.

```
Usage: ./demo.sh [-nlh] [-c PATH] [-m PATH]

Run Cyborg and Mutiny inside a docker container

Options:
  -n          Don't call 'cargo build' on sources
  -l          Repeatedly loop the demonstration
  -c PATH     Path to Cyborg source
  -m PATH     Path to Mutiny source
  -h          Show this message
```

Requirements
------------

You'll need [Docker](https://docker.com) installed and working, and the source for Cyborg and Mutiny. All other requirements are installed and run inside the container.

If you want to be able to run the demo from outside the repository, you'll also need the `realpath` command installed.

```bash
ln -s ./demo.sh ~/.bin/cyborg-demo
```

If you are running the demo on Ubuntu, you may need to enable [memory and swap accounting](https://docs.docker.com/installation/ubuntulinux/#memory-and-swap-accounting).

Useful documentation
--------------------

* [`Dockerfile`](https://docs.docker.com/reference/builder/)
* [`supervisord.conf`](http://supervisord.org/configuration.html#program-x-section-settings)
* [`man multitail`](http://linux.die.net/man/1/multitail)

Licence
-------

The contents of the demo are licenced under the [MIT Licence](http://opensource.org/licenses/MIT).

Authors
-------

Written by [Sam Clements](sam@borntyping.co.uk).
