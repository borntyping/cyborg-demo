#!/bin/bash

set -e

DOCKER_RIEMANN_TAG="cyborg-demo-riemann"
DOCKER_DEMO_TAG="cyborg-demo"

PORT_RIEMANN_PB=4561
PORT_RIEMANN_WS=4562
PORT_SUPERVISOR=4570
PORT_EXAMPLEAPP=4571

function usage() {
    echo "Usage: ./demo.sh [-nlh] [-c PATH] [-m PATH]"
    echo
    echo "Run Cyborg and Mutiny inside a docker container"
    echo
    echo "Options:"
    echo "  -b          Run 'cargo build' on sources"
    echo "  -l          Repeatedly loop the demonstration"
    echo "  -c PATH     Path to Cyborg source"
    echo "  -m PATH     Path to Mutiny source"
    echo "  -h          Show this message"
}

function echo_red() { echo -e "\033[31m$*\033[0m"; }
function echo_green() { echo -e "\033[32m$*\033[0m"; }
function divider() { printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -; }

function build() {
    mkdir -p "./src/build"
    if [[ -n $BUILD ]]; then
        echo_green "Building $1"
        cd "$2"
        cargo build --release
        cd - >/dev/null
        cp "$2/target/release/$1" "./src/build/$1"
    fi

    if [[ ! -f "./src/build/$1" ]]; then
        echo_red "./src/build/$1 does not exist"
        return 1
    fi
}

function wait_for_riemann() {
    echo -n "Waiting for Riemann to start on port $1..."
    for _ in $(seq 1 20); do
        READY=$(riemann-client -P $1 -I 1 query true 1>/dev/null 2>&1; echo $?)
        if [[ $READY == 0 ]]; then
            echo " done."
            return 0
        fi
        sleep 1s
        echo -n "."
    done
    echo " failed to start."
    return 1
}

function cleanup() {
    echo_red "Stopping containers"
    docker stop $DOCKER_RIEMANN_TAG $DOCKER_DEMO_TAG 2>/dev/null || :
    echo_red "Removing containers"
    docker rm $DOCKER_RIEMANN_TAG $DOCKER_DEMO_TAG 2>/dev/null || :
}

function main() {
    if [[ ! $(which docker) ]]; then
        echo_red "docker is not installed"
        echo_red "$ apt-get install docker.io"
        return 1
    fi

    if [[ ! $(which riemann-client) ]]; then
        echo_red "riemann-client is not installed"
        echo_red "$ pip install --user riemann-client"
        return 1
    fi

    build "cyborg" "${CYBORG_SRC:-"/home/sam/Development/borntyping/cyborg"}"
    build "mutiny" "${MUTINY_SRC:-"/home/sam/Development/borntyping/mutiny"}"

    echo_green "Building docker image for $DOCKER_DEMO_TAG"
    docker build -t cyborg-demo "src"

    trap "cleanup" EXIT

    echo_green "Starting Riemann container"
    docker run \
        --publish=$PORT_RIEMANN_PB:5555 \
        --publish=$PORT_RIEMANN_WS:5556 \
        --name=$DOCKER_RIEMANN_TAG -d davidkelley/riemann >/dev/null
    wait_for_riemann $PORT_RIEMANN_PB

    echo_green "Starting cyborg-demo container"
    # This should use `--cap-add=SYS_PTRACE` instead of `--privileged`, but my
    # installed version of docker is too old
    docker run \
        --publish=$PORT_SUPERVISOR:9001 \
        --publish=$PORT_EXAMPLEAPP:5000 \
        --link=$DOCKER_RIEMANN_TAG:riemann \
        --name=$DOCKER_DEMO_TAG --privileged -it cyborg-demo
}

function loop() {
    while [ 1 ]; do
        main
        cleanup
        divider
    done
}

while getopts "blxc:m:h" OPT; do
    case "${OPT}" in
        b) BUILD=1;;
        l) LOOP=1;;
        c) CYBORG_SRC=$OPTARG;;
        m) MUTINY_SRC=$OPTARG;;
        x) cleanup; exit 1;;
        h|*) usage; exit 1;;
    esac
done

# Ensure the demo runs from the correct directory
if [[ ! "$(dirname "$BASH_SOURCE")" == "cyborg-demo" ]]; then
    cd "$(dirname "$(realpath "$BASH_SOURCE")")"
fi

# Build and run the image
if [[ ! -z $LOOP ]]; then
    loop
else
    main
fi
