#!/bin/bash

set -e

function echo_red()   { echo -e "\033[31m$*\033[0m"; }
function echo_blue()  { echo -e "\033[34m$*\033[0m"; }

function recover() {
    echo_red "Dropping to recovery shell (error on line ${LINENO})"
    bash
    return 1
}
trap recover ERR

function wait_for() {
    echo -n "Waiting for $1 to start..."
    for _ in $(seq 1 5); do
        sleep 0.2s
        echo -n "."
        if [[ -f "$2" ]]; then
            echo " done."
            return 0
        fi
    done
    echo " failed to start."
    return 1
}

echo_blue "Starting demonstration"
/usr/local/bin/supervisord -c "/etc/supervisor/supervisord.conf"

wait_for "supervisord"  "/var/log/supervisor/supervisord.log"
wait_for "app"          "/var/log/supervisor/app.log"
wait_for "cyborg"       "/var/log/supervisor/cyborg.stdout.log"
wait_for "mutiny"       "/var/run/mutiny_0.pid"
wait_for "supermann"    "/var/log/supervisor/supermann.log"

echo "Opening multitail view"
multitail -du -s 2 -sn 2, \
    -t "Major Project Demonstration - Sam Clements (sdc2@aber.ac.uk)  " \
        -wh $((1 + $(wc -l < /etc/motd))) -ci blue "/etc/motd" \
    -t "cyborg" "/var/log/supervisor/cyborg.stdout.log" \
        -ci red -I "/var/log/supervisor/cyborg.stderr.log" \
    -t "mutiny [0]" "/var/log/supervisor/mutiny_0.stdout.log" \
    -t "mutiny [1]" "/var/log/supervisor/mutiny_1.stdout.log" \
    -t "mutiny [2]" "/var/log/supervisor/mutiny_2.stdout.log"

echo_blue "Ended demonstration"
