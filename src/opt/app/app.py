#!/usr/bin/python2

import time

from flask import Flask, render_template

start = time.time()
app = Flask(__name__)


@app.route("/")
def index():
    return render_template('index.html', uptime=int(time.time() - start))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, use_reloader=False)
